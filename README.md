# Before you begin

Before you can use this code you have to download and unpack the following packages for windows:

* SDL 2.0 (https://www.libsdl.org/download-2.0.php)
* glew  (http://glew.sourceforge.net/)
* glm (http://glm.g-truc.net/0.9.7/index.html)

After downloading this files you have to declare the following system variables with the path to the corresponding directorys:

* GLEW_DIR - Path to the glew directory
* SDL_DIR - Path to the sdl directory
* GLM_DIR - Path to the glm directory

Now you can start visual studio to open and compile the code.

