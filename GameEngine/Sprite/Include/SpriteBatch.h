#pragma once

#include <vector>
#include <algorithm>

#include <glm.hpp>
#include <GL\glew.h>

#include "Vertex.h"

namespace GameEngine
{
	enum class GlyphSort_t
	{
		NONE,
		FRONT_TO_BACK,
		BACK_TO_FRONT,
		TEXTURE,
	};

	struct Glyph
	{
		GLuint texture;
		float depth;

		Vertex topLeft;
		Vertex bottomLeft;
		Vertex topRight;
		Vertex bottomRight;
	};

	class RenderBatch
	{
		public:
			RenderBatch(GLuint Offset, GLuint NumVertices, GLuint Texture) : offset(Offset), numVertices(NumVertices), texture(Texture) {}
			GLuint offset;
			GLuint numVertices;
			GLuint texture;
	};

	class SpriteBatch
	{
		public:
			SpriteBatch();
			~SpriteBatch();

			void init();

			void begin(GlyphSort_t sortType = GlyphSort_t::TEXTURE);
			void end();

			void draw(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint texture, float depth, const Color& color);

			void renderBatch();

		private:
			void createRenderBatches();
			void createVertexArray();
			void sortGlyphs();

			static bool compareFrontToBack(Glyph* a, Glyph* b);
			static bool compareBackToFront(Glyph* a, Glyph* b);
			static bool compareTexture(Glyph* a, Glyph* b);

			GLuint _vbo;
			GLuint _vao;

			GlyphSort_t _sortType;

			std::vector<Glyph*> _glyphs;
			std::vector<RenderBatch> _renderBachtes;
	};
}

