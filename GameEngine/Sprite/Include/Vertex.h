#pragma once

#include <GL\glew.h>

namespace GameEngine
{
	typedef struct Position_t
	{
		GLfloat x;
		GLfloat y;
	} Position;

	typedef struct Color_t
	{
		GLubyte r;
		GLubyte g;
		GLubyte b;
		GLubyte a;
	} Color;

	typedef struct UV_t
	{
		GLfloat u;
		GLfloat v;
	} UV;

	// Gr��e eines Vertex sollte ein Vielfaches von vier sein
	struct Vertex
	{
		Position position;
		Color color;
		UV uv;

		void setPosition(GLfloat x, GLfloat y)
		{
			position.x = x;
			position.y = y;
		}

		void setColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a)
		{
			color.r = r;
			color.g = g;
			color.b = b;
			color.a = a;
		}

		void setUV(GLfloat u, GLfloat v)
		{
			uv.u = u;
			uv.v = v;
		}
	};
}