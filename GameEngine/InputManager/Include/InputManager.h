#pragma once

#include <unordered_map>

#include <GL\glew.h>

namespace GameEngine
{
	class InputManager
	{
		public:
			InputManager();
			~InputManager();
	
			void pressKey(GLuint keyID);
			void releaseKey(GLuint keyID);

			bool isKeyPressed(GLuint keyID);

		private:
			std::unordered_map<GLuint, bool> _keyMap;
	};
}