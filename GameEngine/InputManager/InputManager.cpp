#include "Include\InputManager.h"

namespace GameEngine
{
	InputManager::InputManager()
	{
	}

	InputManager::~InputManager()
	{
	}

	void InputManager::pressKey(GLuint keyID)
	{
		_keyMap[keyID] = true;
	}

	void InputManager::releaseKey(GLuint keyID)
	{
		_keyMap[keyID] = false;
	}

	bool InputManager::isKeyPressed(GLuint keyID)
	{
		auto it = _keyMap.find(keyID);

		if (it != _keyMap.end())
		{
			return it->second;
		}

		return false;
	}
}
