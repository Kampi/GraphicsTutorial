#pragma once

#include <string>

#include <SDL.h>
#include <GL\glew.h>

#include "..\..\Errors\Include\Errors.h"

namespace GameEngine
{
	enum WindowFlags
	{
		INVISIBLE = 1,
		FULLSCREEN = 2,
		BORDERLESS = 4
	};

	class Window
	{
		public:
			Window();
			~Window();

			int createWindow(std::string windowName, int screenWidth, int screenHeight, unsigned int windowFlags = 0);

			void swapBuffer();

			int getScreenWidth() { return _screenWidth; }
			int getscreenHeight() { return _screenHeight; }

		private:
			SDL_Window* _sdlWindow;
			int _screenWidth;
			int _screenHeight;
	};

}