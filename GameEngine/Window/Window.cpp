#include "Include\Window.h"

namespace GameEngine
{
	Window::Window()
	{
	}

	Window::~Window()
	{
	}

	int Window::createWindow(std::string windowName, int screenWidth, int screenHeight, unsigned int windowFlags)
	{
		Uint32 flags = SDL_WINDOW_OPENGL;

		if (windowFlags & INVISIBLE)
		{
			flags |= SDL_WINDOW_HIDDEN;
		}

		if (windowFlags & FULLSCREEN)
		{
			flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}

		if (windowFlags & BORDERLESS)
		{
			flags |= SDL_WINDOW_BORDERLESS;
		}

		// Erzeuge ein neues Fenster
		_sdlWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);

		if (_sdlWindow == nullptr)
		{
			fatalError("Could not open window!");
		}

		SDL_GLContext p_glContext = SDL_GL_CreateContext(_sdlWindow);

		if (p_glContext == nullptr)
		{
			fatalError("Could not create context!");
		}

		GLenum e_error = glewInit();

		if (e_error != GLEW_OK)
		{
			fatalError("Could not initialize glew!");
		}

		std::cout << "*** OPenGL Version: " << glGetString(GL_VERSION) << std::endl;

		// Hintergrundfarbe setzen
		glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

		// V-Sync deaktivieren
		SDL_GL_SetSwapInterval(0);

		return 0;
	}

	void Window::swapBuffer(void)
	{
		// Buffer tauschen
		SDL_GL_SwapWindow(_sdlWindow);
	}
}