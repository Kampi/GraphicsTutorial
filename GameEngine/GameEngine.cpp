#include <SDL.h>
#include <GL/glew.h>

#include "GameEngine.h"

namespace GameEngine
{
	int GameEngine::init(void)
	{
		// Initialisiere SDL
		SDL_Init(SDL_INIT_EVERYTHING);

		// Double Buffering aktivieren
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		return 0;
	}
}