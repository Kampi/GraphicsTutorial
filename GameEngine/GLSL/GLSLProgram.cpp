#include <vector>
#include <fstream>

#include "..\Errors\Include\Errors.h"
#include "Include\GLSLProgram.h"

namespace GameEngine
{
	GLSLProgram::GLSLProgram() : _numAttributes(0), _programID(0), _vertexShaderID(0), _fragmentShaderID(0)
	{

	}

	GLSLProgram::~GLSLProgram()
	{
	}

	// Kompiliert alle Shader-Programme
	void GLSLProgram::compileShaders(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath)
	{
		// Erzeuge ein Shader Programm
		_programID = glCreateProgram();

		_vertexShaderID = glCreateShader(GL_VERTEX_SHADER);

		if (_vertexShaderID == 0)
		{
			fatalError("Can not create vertex shader");
		}

		_fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

		if (_fragmentShaderID == 0)
		{
			fatalError("Can not create fragment shader");
		}

		compileShader(vertexShaderFilePath, _vertexShaderID);
		compileShader(fragmentShaderFilePath, _fragmentShaderID);
	}

	// Linkt alle Shader-Programme
	void GLSLProgram::linkShaders(void)
	{
		GLint isLinked = 0;

		glAttachShader(_programID, _vertexShaderID);
		glAttachShader(_programID, _fragmentShaderID);

		glLinkProgram(_programID);

		glGetProgramiv(_programID, GL_LINK_STATUS, (int*)&isLinked);

		if (isLinked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(_programID, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(_programID, maxLength, &maxLength, &errorLog[0]);

			glDeleteShader(_vertexShaderID);
			glDeleteShader(_fragmentShaderID);

			std::printf("%s\n", &(errorLog[0]));
			fatalError("Shaders failed to link!");
		}

		glDetachShader(_programID, _vertexShaderID);
		glDetachShader(_programID, _fragmentShaderID);
		glDeleteShader(_vertexShaderID);
		glDeleteShader(_fragmentShaderID);
	}

	// Ein Attribut zum Shader hinzuf�gen
	// Muss zwischen dem Kompilieren und dem Linken des Shaders aufgerufen werden
	void GLSLProgram::addAttribute(const std::string& attributeName)
	{
		glBindAttribLocation(_programID, _numAttributes, attributeName.c_str());
		_numAttributes++;
	}

	// Gibt einen Handle auf eine uniform-Variable aus dem Shader-Programm zur�ck
	GLint GLSLProgram::getUniformLocation(const std::string& uniformName)
	{
		GLint location = glGetUniformLocation(_programID, uniformName.c_str());

		if (location == GL_INVALID_INDEX)
		{
			fatalError("Uniform " + uniformName + " not found in shader!");
		}

		return location;
	}

	// Aktiviert den Shader und all seine Attribute
	void GLSLProgram::use(void)
	{
		glUseProgram(_programID);

		for (int i = 0; i < _numAttributes; i++)
		{
			glEnableVertexAttribArray(i);
		}
	}

	// Deaktiviert den Shader und all seine Attribute
	void GLSLProgram::unuse(void)
	{
		glUseProgram(0);

		for (int i = 0; i < _numAttributes; i++)
		{
			glDisableVertexAttribArray(i);
		}
	}

	// Unterprogramm um einen Shader zu kompilieren
	void GLSLProgram::compileShader(const std::string& filePath, GLuint id)
	{
		std::string line;
		std::string fileContents = "";

		// Lade shader file
		std::ifstream shaderFile(filePath);
		if (shaderFile.fail())
		{
			perror(filePath.c_str());
			fatalError("Failed to open " + filePath);
		}

		while (std::getline(shaderFile, line))
		{
			fileContents += line + "\n";
		}

		shaderFile.close();

		// Kompiliere shader file
		GLint success = 0;
		const char* contentsPtr = fileContents.c_str();
		glShaderSource(id, 1, &contentsPtr, nullptr);
		glCompileShader(id);
		glGetShaderiv(id, GL_COMPILE_STATUS, &success);

		// Error handling
		if (success == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(id, maxLength, &maxLength, &errorLog[0]);

			glDeleteShader(id);

			std::printf("%s\n", &(errorLog[0]));
			fatalError("Shader " + filePath + " failed to compile!");
		}
	}
}