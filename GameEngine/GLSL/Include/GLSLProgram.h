#pragma once

#include <string>
#include <GL/glew.h>

namespace GameEngine
{
	class GLSLProgram
	{
		public:
			GLSLProgram();
			~GLSLProgram();

			void compileShaders(const std::string&, const std::string&);
			void linkShaders();
			void addAttribute(const std::string&);

			GLint getUniformLocation(const std::string&);

			void use();
			void unuse();

		private:

			int _numAttributes;

			void compileShader(const std::string&, GLuint);

			GLuint _programID;
			GLuint _vertexShaderID;
			GLuint _fragmentShaderID;
	};
}
