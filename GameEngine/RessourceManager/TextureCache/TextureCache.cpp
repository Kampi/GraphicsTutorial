#include "Include\TextureCache.h"

namespace GameEngine
{
	TextureCache::TextureCache()
	{
	}

	TextureCache::~TextureCache()
	{
	}

	GLTexture TextureCache::getTexture(std::string texturePath)
	{
		auto mit = _textureMap.find(texturePath);

		// Ist die Textur bereits vorhanden?
		if (mit == _textureMap.end())
		{
			// Lade neue Textur
			GLTexture newTexture = ImageLoader::loadPNG(texturePath);

			// F�ge Textur dem Mapping hinzu
			_textureMap.insert(make_pair(texturePath, newTexture));

			std::cout << "Loaded texture" << std::endl;

			return newTexture;
		}

		std::cout << "Loaded chached texture" << std::endl;
		return mit->second;
	}
}