#pragma once

#include <map>
#include <string>
#include <iostream>
#include <algorithm>

#include "..\Textures\GLTexture.h"
#include "..\Textures\ImageLoader\Include\ImageLoader.h"

namespace GameEngine
{
	class TextureCache
	{
		public:
			TextureCache();
			~TextureCache();

			GLTexture getTexture(std::string texturePath);

		private:
			std::map<std::string, GLTexture> _textureMap;
	};
}


