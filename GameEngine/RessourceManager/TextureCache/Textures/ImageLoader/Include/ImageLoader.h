#pragma once

#include <string>
#include <vector>

#include "..\..\GLTexture.h"
#include "..\picoPNG\Include\picopng.h"
#include "..\IOManager\Include\IOManager.h"
#include "..\..\..\..\..\Errors\Include\Errors.h"

namespace GameEngine
{
	static class ImageLoader
	{
		public:
			static GLTexture loadPNG(std::string filePath);

		private:

	};
}

