#include "Include\ImageLoader.h"

namespace GameEngine
{
	GLTexture ImageLoader::loadPNG(std::string filePath)
	{
		// Wird aus der Funktion �bergeben
		// Nicht der optimalste Weg, da das komplette Struct kopiert wird
		GLTexture texture = {};

		std::vector<unsigned char> in;
		std::vector<unsigned char> out;
		unsigned long width;
		unsigned long height;

		// Datei einlesen und Inhalt speichern
		if (IOManager::readFileToBuffer(filePath, in) == false)
		{
			fatalError("Failed to load PNG file to buffer!");
		}

		// PNG dekodieren
		int errorCode = decodePNG(out, width, height, &(in[0]), in.size());

		if (errorCode != 0)
		{
			fatalError("Decode PNG failed with error: " + std::to_string(errorCode));
		}

		// Erzeuge die Textur und weise ihr eine eindeutige ID zu
		glGenTextures(1, &(texture.id));

		// Binde eine 2D Textur
		glBindTexture(GL_TEXTURE_2D, texture.id);

		// Lade das Bild in die aktuelle Open GL Textur hoch
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &(out[0]));

		// Texturparameter setzen
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// Mipmap erzeugen
		glGenerateMipmap(GL_TEXTURE_2D);

		// L�se die Bindung
		glBindTexture(GL_TEXTURE_2D, 0);

		texture.width = width;
		texture.height = height;

		return texture;
	}
}