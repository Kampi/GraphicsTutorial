#pragma once

#include <vector>
#include <fstream>

namespace GameEngine
{
	static class IOManager
	{
		public:
			static bool readFileToBuffer(std::string, std::vector<unsigned char>&);

		private:

	};
}

