#include "Include\IOManager.h"

namespace GameEngine
{
	// Lie�t eine Datei bin�r ein und speichert den Inhalt in einem Buffer
	bool IOManager::readFileToBuffer(std::string filePath, std::vector<unsigned char>& buffer)
	{
		std::ifstream file(filePath, std::ios::binary);

		if (file.fail())
		{
			perror(filePath.c_str());
			return false;
		}

		// Cursor an das Ende der Datei setzen
		file.seekg(0, std::ios::end);

		// Dateigr��e ermitteln
		int fileSize = file.tellg();

		// Cursor an den Anfang der Datei setzen
		file.seekg(0, std::ios::beg);

		// Dateiheader entfernen
		fileSize -= file.tellg();

		buffer.resize(fileSize);
		file.read((char*)&(buffer[0]), fileSize);

		file.close();

		return true;
	}
}