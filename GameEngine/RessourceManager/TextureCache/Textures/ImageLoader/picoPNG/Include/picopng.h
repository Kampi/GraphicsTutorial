#pragma once

#include <vector>

namespace GameEngine
{
	extern int decodePNG(std::vector<unsigned char>&, unsigned long&, unsigned long&, const unsigned char*, size_t, bool convert_to_rgba32 = true);
}