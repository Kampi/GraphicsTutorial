#include "Include\RessourceManager.h"

namespace GameEngine
{
	TextureCache RessourceManager::_textureCache;

	GLTexture RessourceManager::getTexture(std::string texturePath)
	{
		return _textureCache.getTexture(texturePath);
	}
}