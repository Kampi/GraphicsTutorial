#pragma once

#include <string>

#include "..\TextureCache\Include\TextureCache.h"

namespace GameEngine
{
	static class RessourceManager
	{
		public:
			static GLTexture getTexture(std::string texturePath);

		private:
			static TextureCache _textureCache;
	};
}