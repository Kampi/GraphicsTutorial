#version 130

// Eingang vom Vertexbuffer. Es werden x- und y-Koordinaten 
// und vier Farbkanäle verwendet
in vec2 vertexPosition;
in vec4 vertexColor;
in vec2 vertexUV;

// Ausgang vom Vertex-Shader
// Wird direkt zum Fragment-Shader gesendet
out vec2 fragmentPosition;
out vec4 fragmentColor;
out vec2 fragmentUV;

// Orthogonale Projektionsmatrix
uniform mat4 P;

void main()
{
    //Set the x,y position on the screen
    gl_Position.xy = (P * vec4(vertexPosition, 0.0, 1.0)).xy;
    gl_Position.z = 0.0;

	fragmentColor = vertexColor;

	fragmentPosition = vertexPosition;

	// Bild drehen
	fragmentUV = vec2(vertexUV.x, 1.0 - vertexUV.y);
}