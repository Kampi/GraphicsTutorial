#version 130

// Eingang vom Fragment-Shader
// Kommt direkt vom Vertex-Shader
in vec2 fragmentPosition;
in vec4 fragmentColor;
in vec2 fragmentUV;

// Ausgang vom Fragment-Shader
out vec4 color;

uniform float time;
uniform sampler2D mySampler; 

void main() 
{
	vec4 textureColor = texture(mySampler, fragmentUV);

	color = vec4(	fragmentColor.r * (cos(fragmentPosition.x * 4.0 + time) + 1.0) * 0.5,
					fragmentColor.g * (cos(fragmentPosition.x * 8.0  + time) + 1.0) * 0.5,
					fragmentColor.b * (cos(fragmentPosition.x * 1.0  + time) + 1.0) * 0.5,
					1.0
					) * textureColor;
}