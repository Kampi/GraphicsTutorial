#include "Include\Timing.h"

namespace GameEngine
{
	FpsLimiter::FpsLimiter()
	{
	}

	FpsLimiter::~FpsLimiter()
	{
	}

	void FpsLimiter::setMaxFps(GLfloat maxFps)
	{
		_maxFps = maxFps;
	}

	void FpsLimiter::init(GLfloat maxFps)
	{
		setMaxFps(maxFps);
	}

	void FpsLimiter::beginFrame(void)
	{
		_startTicks = SDL_GetTicks();
	}

	GLfloat FpsLimiter::end(void)
	{
		calculateFps();

		// Frame-Zeit messen
		float frameTicks = SDL_GetTicks() - _startTicks;

		// FPS limitieren
		if (1000.0 / _maxFps > frameTicks)
		{
			// Frame verzögern
			SDL_Delay(1000.0 / _maxFps - frameTicks);
		}

		return _fps;
	}

	void FpsLimiter::calculateFps(void)
	{
		static const GLint NUM_SAMPLES = 10;
		static GLfloat frameTimes[NUM_SAMPLES];
		static GLint currentFrame = 0;
		GLint count = NUM_SAMPLES;
		static GLfloat prevTicks = SDL_GetTicks();

		GLfloat currentTicks;

		currentTicks = SDL_GetTicks();

		_frameTime = currentTicks - prevTicks;
		frameTimes[currentFrame % NUM_SAMPLES] = _frameTime;

		prevTicks = currentTicks;

		currentFrame++;
		if (currentFrame < NUM_SAMPLES)
		{
			count = currentFrame;
		}

		GLfloat frameTimeAverage = 0.0;

		for (int i = 0; i < count; i++)
		{
			frameTimeAverage += frameTimes[i];
		}

		frameTimeAverage /= count;

		if (frameTimeAverage > 0.0)
		{
			_fps = 1000.0 / frameTimeAverage;
		}
	}
}