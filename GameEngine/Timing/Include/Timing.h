#pragma once

#include <SDL.h>
#include <GL\glew.h>

namespace GameEngine
{
	class FpsLimiter
	{
		public:
			FpsLimiter();
			~FpsLimiter();

			void setMaxFps(GLfloat maxFps);
			void init(GLfloat maxFps);
			void beginFrame();
			GLfloat end();

		private:
			void calculateFps();

			GLfloat _fps;
			GLfloat _frameTime;

			GLfloat _maxFps;
			GLuint _startTicks;
	};
}