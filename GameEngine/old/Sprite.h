#pragma once

#include <string>
#include <cstddef>
#include <GL\glew.h>

#include "Vertex.h"
#include "Textures\GLTexture.h"
#include "RessourceManager\Include\RessourceManager.h"

#define NO_BUFFER	1
#define NO_VERTEX	6

namespace GameEngine
{
	class Sprite
	{
		public:
			Sprite();
			~Sprite();

			void init(float x, float y, float width, float heigth, std::string texturePath);
			void draw();

		private:
			float _x;
			float _y;
			float _width;
			float _height;

			// Vertex-Buffer ID
			// Verwende GL-Datentypen
			GLuint _vboID;

			GLTexture _texture;
	};
}

