#include "Sprite.h"

namespace GameEngine
{
	Sprite::Sprite() : _vboID(0)
	{
	}

	Sprite::~Sprite()
	{
		if (_vboID != 0)
		{
			glDeleteBuffers(NO_BUFFER, &_vboID);
		}
	}

	void Sprite::init(float x, float y, float width, float heigth, std::string texturePath)
	{
		_x = x;
		_y = y;
		_width = width;
		_height = heigth;

		_texture = RessourceManager::getTexture(texturePath);

		// Erzeuge Vertex-Buffer
		if (_vboID == 0)
		{
			glGenBuffers(NO_BUFFER, &_vboID);
		}

		// 6 Vertex f�r ein Viereck
		Vertex vertexData[NO_VERTEX];

		// Erstes Dreieck
		vertexData[0].setPosition(_x + _width, _y + _height);
		vertexData[0].setUV(1.0f, 1.0f);

		vertexData[1].setPosition(_x, _y + _height);
		vertexData[1].setUV(0.0f, 1.0f);

		vertexData[2].setPosition(_x, _y);
		vertexData[2].setUV(0.0f, 0.0f);

		// Zweites Dreieck
		vertexData[3].setPosition(_x, _y);
		vertexData[3].setUV(0.0f, 0.0f);

		vertexData[4].setPosition(_x + _width, _y);
		vertexData[4].setUV(1.0f, 0.0f);

		vertexData[5].setPosition(_x + _width, _y + _height);
		vertexData[5].setUV(1.0f, 1.0f);

		// Jedem Vertex eine Farbe zuweisen
		for (int i = 0; i < NO_VERTEX; i++)
		{
			vertexData[i].setColor(255, 0, 255, 255);
		}

		vertexData[1].setColor(0, 0, 255, 255);
		vertexData[4].setColor(0, 255, 0, 255);

		glBindBuffer(GL_ARRAY_BUFFER, _vboID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void Sprite::draw(void)
	{
		// Binde die Textur mit der �bergebenen ID, damit sie gezeichnet werden kann
		glBindTexture(GL_TEXTURE_2D, _texture.id);

		// Grafikkarte mitteilen, dass dieser Buffer verwendet wird
		// Es kann immer nur ein Buffer gleichzeitig aktiv sein
		glBindBuffer(GL_ARRAY_BUFFER, _vboID);

		// Daten zeichnen
		glDrawArrays(GL_TRIANGLES, 0, NO_VERTEX);

		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);

		// Buffer freigeben
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}