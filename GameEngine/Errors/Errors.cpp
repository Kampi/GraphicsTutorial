#include "Include\Errors.h"

namespace GameEngine
{
	void fatalError(std::string errorString)
	{
		std::cout << errorString << std::endl;
		std::cout << "Press any key to quit..." << std::endl;
		system("PAUSE");

		SDL_Quit();
		exit(-1);
	}
}