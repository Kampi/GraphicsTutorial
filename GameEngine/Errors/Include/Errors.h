#pragma once

#include <string>
#include <cstdlib>
#include <iostream>

#include <SDL.h>

namespace GameEngine
{
	extern void fatalError(std::string);
}