#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <Windows.h>

#include <SDL.h>
#include <GL\glew.h>
#include <glm.hpp>
#include <gtc\matrix_transform.hpp>

#include <GameEngine\GameEngine.h>
#include <GameEngine\Window\Include\Window.h>
#include <GameEngine\Timing\Include\Timing.h>
#include <GameEngine\Camera\Include\Camera2D.h>
#include <GameEngine\GLSL\Include\GLSLProgram.h>
#include <GameEngine\Sprite\Include\SpriteBatch.h>
#include <GameEngine\InputManager\Include\InputManager.h>
#include <GameEngine\RessourceManager\Include\RessourceManager.h>

using namespace std;
using namespace glm;
using namespace GameEngine;

class MainGame
{
	enum class GameState_t
	{
		State_Play,
		State_Exit
	};

	public:
		MainGame();
		~MainGame();

		void run();

	private:
		Window _window;

		GLTexture _playerTexture;
		GLSLProgram _colorProgram;
		Camera2D _camera;
		SpriteBatch _spriteBatch;
		InputManager _inputManager;
		FpsLimiter _fpsLimiter;

		int _screenWidth;
		int _screenHeight;

		GameState_t _gameState;

		void initSystems();
		void initShaders();
		void gameLoop();
		void processInput();
		void drawGame();

		float _fps;
		float _maxFPS;
		float _time;

	protected:

};

