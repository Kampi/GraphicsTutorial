#include "MainGame.h"


MainGame::MainGame() : _time(0.0), _maxFPS(60.0)
{
	_screenWidth = 1024;
	_screenHeight = 768;
	_gameState = GameState_t::State_Play;
	_camera.init(_screenWidth, _screenHeight);
}

MainGame::~MainGame()
{
}

void MainGame::run(void)
{ 
	this->initSystems();
	this->gameLoop();
}



void MainGame::initSystems(void)
{
	init();
	_window.createWindow("Game Engine", _screenWidth, _screenHeight, 0);

	this->initShaders();
	_spriteBatch.init();
	_fpsLimiter.init(_maxFPS);
}

void MainGame::initShaders()
{
	_colorProgram.compileShaders("D:/Dropbox/GraphicsTutorial/GameEngine/Shader/colorShading.vert", "D:/Dropbox/GraphicsTutorial/GameEngine/Shader/colorShading.frag");
	_colorProgram.addAttribute("vertexPosition");
	_colorProgram.addAttribute("vertexColor");
	_colorProgram.addAttribute("vertexUV");
	_colorProgram.linkShaders();
}

void MainGame::gameLoop(void)
{
	while (_gameState != GameState_t::State_Exit)
	{
		_fpsLimiter.beginFrame();

		this->processInput();
		_time += 0.01f;

		_camera.update();
		this->drawGame();

		_fps = _fpsLimiter.end();

		static int frameCounter = 0;
		frameCounter++;
		if (frameCounter == 10)
		{
			std::cout << _fps << std::endl;
			frameCounter = 0;
		}
	}

	SDL_Quit();
}

void MainGame::processInput(void)
{
	SDL_Event p_Event;
	
	const float CAMERA_SPEED = 20.0f;
	const float SCALE_SPEED = 0.1f;

	while (SDL_PollEvent(&p_Event))
	{
		switch (p_Event.type)
		{
			case SDL_QUIT:
			{
				_gameState = GameState_t::State_Exit;
				break;
			}
			case SDL_MOUSEMOTION:
			{
				//cout << "x: " << p_Event.motion.x << endl;
				//cout << "y: " << p_Event.motion.y << endl;
				break;
			}
			case SDL_KEYDOWN:
			{
				_inputManager.pressKey(p_Event.key.keysym.sym);
				break;
			}
			case SDL_KEYUP:
			{
				_inputManager.releaseKey(p_Event.key.keysym.sym);
				break;
			}
		}
	}

	if (_inputManager.isKeyPressed(SDLK_w))
	{
		_camera.setPosition(_camera.getPosition() + glm::vec2(0.0, CAMERA_SPEED));
	}
	if (_inputManager.isKeyPressed(SDLK_s))
	{
		_camera.setPosition(_camera.getPosition() + glm::vec2(0.0, -CAMERA_SPEED));
	}
	if (_inputManager.isKeyPressed(SDLK_a))
	{
		_camera.setPosition(_camera.getPosition() + glm::vec2(CAMERA_SPEED, 0.0));
	}
	if (_inputManager.isKeyPressed(SDLK_d))
	{
		_camera.setPosition(_camera.getPosition() + glm::vec2(-CAMERA_SPEED, 0.0));
	}
	if (_inputManager.isKeyPressed(SDLK_q))
	{
		_camera.setScale(_camera.getScale() + SCALE_SPEED);
	}
	if (_inputManager.isKeyPressed(SDLK_i))
	{
		_camera.setScale(_camera.getScale() - SCALE_SPEED);
	}
}

void MainGame::drawGame(void)
{
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Shader aktivieren
	_colorProgram.use();

	// Aktiviere Textur 0
	glActiveTexture(GL_TEXTURE0);

	// Zugriff auf die uniform-Variable "mySampler" erzeugen
	GLuint textureLocation = _colorProgram.getUniformLocation("mySampler");

	// Teile dem Shader mit, dass die Textur in Texturlocation 0 liegt
	glUniform1i(textureLocation, 0);

	// Handle auf uniform-Variable "time" erzeugen
	GLuint timeLocation = _colorProgram.getUniformLocation("time");

	// Handle auf uniform-Variable "P" erzeugen
	GLuint pLocation = _colorProgram.getUniformLocation("P");
	mat4 cameraMatrix = _camera.getCameraMatrix();

	// Matrix in die GPU laden
	glUniformMatrix4fv(pLocation, 1, GL_FALSE, &(cameraMatrix[0][0]));

	_spriteBatch.begin();

	glm::vec4 pos(0.0, 0.0, 50.0, 50.0);
	glm::vec4 uv(0.0, 0.0, 1.0, 1.0);
	static GLTexture texture = RessourceManager::getTexture("Images/PNG/CharacterRight_Standing.png");
	Color color;
	color.r = 255;
	color.g = 255;
	color.b = 255;
	color.a = 255;

	_spriteBatch.draw(pos, uv, texture.id, 0.0, color);

	_spriteBatch.end();

	_spriteBatch.renderBatch();

	// Setze den Wert der uniform-Variable
	glUniform1f(timeLocation, _time);

	// Texturbindung l�sen
	glBindTexture(GL_TEXTURE_2D, 0);

	// Shader deaktivieren
	_colorProgram.unuse();

	_window.swapBuffer();
}
